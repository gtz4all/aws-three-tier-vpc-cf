AWSTemplateFormatVersion: 2010-09-09
Description: GTZ4All Three Tier VPC Nested Conditions Template
Metadata: 
  AWS::CloudFormation::Interface: 
    ParameterGroups:
      - Label: 
          default: "Stack Name - {{ProjectName}}-{{Environment}}-three-tier-vpc-cf (i.e jira-dev-three-tier-vpc-cf)"
      - Label: 
          default: "|"
      - Label: 
          default: "VPC Configuration"
        Parameters:
          - ProjectName
          - Environment          
          - CidrBlock
      - Label: 
          default: "Subnet 8 = /24(/20 CidrBlock Required), 7 = /25 (/21 CidrBlock Required), 6 = /26 (/22 CidrBlock Required)"
        Parameters:
          - CidrMask
          
      - Label: 
          default: "Availability Zones"
        Parameters:
          - NumberOfAZs

Parameters:
  ProjectName:
    Description: Project Name ( ex. jira /  sfpt / sec )
    Type: String
    
  CidrBlock1:
    ConstraintDescription: CIDR block parameter must be in the form x.x.x.x
    Description: VPC valid IP Block ( ex. 10.168.0.0)
    Default: 10.168.0.0
    Type: String
    
    
  Cidr:
    Type: Number
    AllowedValues: 
    - 20
    - 22
    - 21
    Default: 20
    Description:  VPC valid IP CIDR Blocks /20's ( /24's smaller blocks )  , /21's ( /25's smaller blocks )and /22's ( /26's smaller blocks )
    
  NumberOfAZs:
    Type: Number
    AllowedValues: 
    - 2
    - 4
    Default: 2
    Description:  Availability Zones - 2 = 2 per Tier, 4 = 4 per Tier
    
  Environment:
    Description: Choose VPC Type 
    Type: String
    AllowedValues: 
    - dev
    - test
    - prod
    
Conditions: 
  Build4AZs:    !Equals [ !Ref NumberOfAZs, 4 ]
  CidrBit8:    !Equals [ !Ref Cidr, 20 ]
  CidrBit7:    !Equals [ !Ref Cidr, 21 ]
  CidrBit6:    !Equals [ !Ref Cidr, 22 ]

###############################################
### Three Tier VPC                         ####
###############################################  
Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Join ['', [!Ref CidrBlock1, "/", !Ref Cidr ]]
      EnableDnsHostnames: true
      EnableDnsSupport: true
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName,!Ref Environment, "three-tier", "vpc" ]]
        - Key: Type
          Value: "SharedVPC"

  DhcpOptions: 
      Type: AWS::EC2::DHCPOptions
      Properties: 
          DomainName: !Join ['.', [!Ref Environment, "gtz4all.com" ]]
          DomainNameServers: 
            - AmazonProvidedDNS
          Tags: 
            - Key: Name
              Value: !Join ['-', [!Ref ProjectName,!Ref Environment, "three-tier", "dopt" ]]

  VPCDHCPOptionsAssociation:
    Type: AWS::EC2::VPCDHCPOptionsAssociation
    Properties:
      VpcId: {Ref: VPC}
      DhcpOptionsId: {Ref: DhcpOptions}
    
  InternetGateway:
    Type: AWS::EC2::InternetGateway
    DependsOn: VPC
    Properties:
      Tags:
      - Key: Name
        Value: !Join ['-', [!Ref ProjectName,!Ref Environment, "three-tier", "igw" ]]
  VPCGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway

###############################################
### Public/ALB Tier (inbound/outbound)     ####
############################################### 
  PublicRouteTableA:
    Type: AWS::EC2::RouteTable
    DependsOn: VPCGatewayAttachment
    Properties:
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName, "public", "rt-1a" ]]
      VpcId: !Ref VPC
  PublicRouteTableB:
    Type: AWS::EC2::RouteTable
    DependsOn: VPCGatewayAttachment
    Properties:
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName, "public", "rt-1b" ]]
      VpcId: !Ref VPC       
###############################################
  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select [0, !GetAZs '']
      CidrBlock: !Select [ 0, !Cidr [ !Join ['', [!Ref CidrBlock1, "/", !Ref Cidr ]], 12, !If [CidrBit8, 8, !If [CidrBit7, 7, 6]]]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName, "public", "subnet-1a" ]]
      VpcId: !Ref VPC
  PublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select [1, !GetAZs '']     
      CidrBlock: !Select [ 1, !Cidr [ !Join ['', [!Ref CidrBlock1, "/", !Ref Cidr ]], 12, !If [CidrBit8, 8, !If [CidrBit7, 7, 6]]]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName, "public", "subnet-1b" ]]
      VpcId: !Ref VPC
  PublicSubnet3:
    Type: AWS::EC2::Subnet
    Condition: Build4AZs
    Properties:
      AvailabilityZone: !Select [2, !GetAZs '']
      CidrBlock: !Select [ 2, !Cidr [ !Join ['', [!Ref CidrBlock1, "/", !Ref Cidr ]], 12, !If [CidrBit8, 8, !If [CidrBit7, 7, 6]]]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName, "public", "subnet-1c" ]]
      VpcId: !Ref VPC
  PublicSubnet4:
    Type: AWS::EC2::Subnet
    Condition: Build4AZs
    Properties:
      AvailabilityZone: !Select [3, !GetAZs '']     
      CidrBlock: !Select [ 3, !Cidr [ !Join ['', [!Ref CidrBlock1, "/", !Ref Cidr ]], 12, !If [CidrBit8, 8, !If [CidrBit7, 7, 6]]]]
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref ProjectName, "public", "subnet-1d" ]]
      VpcId: !Ref VPC
###############################################
  PublicSubnetAssoc1:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTableA
      SubnetId: !Ref PublicSubnet1
  PublicSubnetAssoc2:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PublicRouteTableB
      SubnetId: !Ref PublicSubnet2
  PublicSubnetAssoc3:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Condition: Build4AZs
    Properties:
      RouteTableId: !Ref PublicRouteTableA
      SubnetId: !Ref PublicSubnet3
  PublicSubnetAssoc4:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Condition: Build4AZs
    Properties:
      RouteTableId: !Ref PublicRouteTableB
      SubnetId: !Ref PublicSubnet4
