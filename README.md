## AWS Three-Tier VPC Design"

An AWS Three-Tier VPC Design provides a baseline to deploy software applications that consist of frontend, backend and database.This design provides Availability Zone High Availability and Fault Tolerant. In addition this design also provides the required topoligy for a highly secured environment.

![Gtz4all Three-tier VPC Design](images/gtz4all-three-tier-vpc-design.jpeg)

#### Public Tier
The Public Tier has both inbound/outbound VPC External access. It is used to provide external services and an entry point into the environment

***Recommended Resources***:
* Public Application Load Balancer
    Users will only be allowed to access application via public Application Load Balancer
* NAT Gateways
    Provides outbound access for private tier. This is required for software updates or patches
* Bastion Hosts
    A bastion host provides secure management access into all three tiers. These servers should be secured with MFA. 

#### Private Tier
The Private Tier only has outbound VPC External Access which is mainly used for software patches and updates. This is where the application servers and internal load balancer reside.

***Recommended Resources***:
* Internal Application Load Balancer
    This type of ALBs and NLBs are used to provide a Highly Available Service to the Public Tier ALBs
* Application Servers
    EC2s, Lambda or EKS, ECS

#### Local Tier
This tier does not have inbound/outbould VPC External Access. It is meant to host database services. Services inside this tier should only be access by the application services. 

***Recommended Resources***:
* Database Instances

## Cloudformation
Cloudformation is a tool to quickly and consistently deploy infrastructure by building the designed resources and managing them as stacks. it is a declarative programming language with some basic features specific to this use case.

#### Componentes
* Metadata: 

  ```AWS::CloudFormation::Interface``` is used to modify how the AWS Cloudformation groups, labels and orders user input parameters.
  https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/metadata-section-structure.html 
  ```yaml
  Metadata: 
  AWS::CloudFormation::Interface: 
    ParameterGroups:
      - Label: 
          default: "Availability Zones"
        Parameters:
          - NumberOfAZs
  ```
* Parametes:

  Parameters are used to request user input custom values which are later used by the ```Resources``` section to build infrastructure.
  https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/parameters-section-structure.html
  ```yaml
  Parameters:
    NumberOfAZs:
      Type: Number
      AllowedValues: 
      - 2
      - 4
      Default: 2
  ```

* Conditions:

  Conditions are used to set parameters based on other parameter's values provided by the user. These conditions are later used to set conditial resources. ex: If ```Condition: Value``` is true, create resource.
  https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/conditions-section-structure.html
  ```yaml
  ## Conditions are either True or False and are used to choose whether a rosource should be created or not. 
  Conditions: 
    ## Build4AZs is True if NumberofAZs is set to '4'
    Build4AZs:    !Equals [ !Ref NumberOfAZs, 4 ] 
    ## Build4AZs is True if NumberofAZs is set to '4'
    ProdVPC:    !Equals [ !Ref Environment, prod ] 
    ## BuildProdEnvironemt is True if both Build4AZs and ProdVPC are True
    BuildProdEnvironemt:   !And [ !Condition Build4AZs, !Condition ProdVPC ]

  Resources:
    PublicSubnet3:
      Type: AWS::EC2::Subnet
      ## PublicSubnet3 will be created only if Build4AZs is True
      Condition: Build4AZs
  ```

#### Built-in Intrinsic Functions

An intrinsic function used to perform a mathematical, character, or logical operation against a data item whose value is derived automatically during execution.

###### Fn::Cidr ( !Cidr )

  This Subnetting calculator function returns an CIDR address block list based on the ```count`` provided. 

  ``` !Cidr [ ipBlock, count, cidrBits ] ```

  * ***Parameters***
    - ```ipBlock```: CIDR address block to be split into smaller CIDR blocks.
    - ```count```: The number of smaller CIDRs blocks to generate. Valid range 1 - 256.
    - ```cidrBits```: The number of subnet bits for the CIDR. 


  * ***Example***
    - Requirementes: create 12(count) /24s(cidrBits) out of a /16 (ipBlock) and pick the 6th(!Select) CIDR block.
    
<table>
<tr>
<th>!Select [ 5, !Cidr [ "10.10.0.0/16", 12, 5 ]</th>
<th>Generated Table</th>
</tr>
<tr>
<td>

  - !Select - select the 6th block - starts from 0 - 7 if there are 8 blocks.

    ```!Select [ 5,``` 
  - !Cidr [CidrBlock = (/16),  12 = break /16 into 12 block, what size for each block? look at host-bits...

    ```!Cidr [ "10.10.0.0/16", 12,```
  - SubnetBits/cidrBits = 8 - which means host-bits of 8, (/24s)
  
    ```8]]```

***cidrBit Table***

Suffix|IPs|CIDR|Borrowed Bits|Binary
-|-|-|-|-
.255|1  |/32|0|11111111
.254|2  |/31|1|11111110
.252|4  |/30|2|11111100
.248|8  |/29|3|11111000
.240|16 |/28|4|11110000
.224|32 |/27|5|11100000
.192|64 |/26|6|11000000
.128|128|/25|7|10000000
.0|256|/24|8|00000000

</td>
<td>    

Count|CidrBlocks|Select
-|-|-
0|10.10.0.0/24| 
1|10.10.1.0/24| 
2|10.10.2.0/24| 
3|10.10.3.0/24| 
4|10.10.4.0/24| 
5|10.10.5.0/24|!Select
6|10.10.6.0/24| 
7|10.10.7.0/24| 
8|10.10.8.0/24| 
9|10.10.9.0/24| 
10|10.10.10.0/24|
11|10.10.11.0/24|
12|10.10.12.0/24| 

</td>
</tr>
</table>

  * ***Conditional Subnetting***
    ```yaml
    !Select [ !If [BuildProd, 0, 0], !Cidr [ !Ref CidrBlock, !If [BuildProd, 16, 8], !If [BuildProd, 5, 6]]]
    ```
<table>
<tr>
<th>Conditial !Cidr</th>
<th>Condition !if</th>
</tr>
<tr>
<td>

  - if BuildPord is True - Create 16 /27s (Host-Bits - 5 ) blocks
  - if BuildPord is False - Create 8 /26s (Host-Bits - 6 ) blocks 

    ```yaml
    !Select [ !If [BuildProd, 0, 0], 
    !Cidr [ !Ref CidrBlock, !If [BuildProd, 16, 8], 
    !If [BuildProd, 5, 6]]]
    ```

</td>
<td>

  !If Condition|Boolean|Value
  -|-|-
  BluidProd|True|firstValue
  BluidProd|False|secondValue

  ***!Cidr*** 

  ipBlock|count|cidrBits
  -|-|-
  !Ref CidrBlock|!If [BuildProd, 16, 8]|!If [BuildProd, 5, 6]
    
</td>
</tr>
</table>

###### Fn::GetAZs (!GetAZs)

The intrinsic function Fn::GetAZs returns a Availability Zone Name List based for a region. 

{{% notice note %}}
AZ Name to AZ ID mapping is different per account.
{{% /notice %}}

* ```!GetAZs ""``` in ```us-east-1``` returns a list of all Availability Zones: ```[ "us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f" ]``` 

  0|1|2|3|4|5
  -|-|-|-|-|-
  "us-east-1a"|"us-east-1b"|"us-east-1c"|"us-east-1d"|"us-east-1e"|"us-east-1f"

* ```!Select``` can pick an AZ based on the order list. 

  ```!Select [3, !GetAZs '']``` returns  ```"us-east-1d"```.


#### Three Tier VPC Cloudformation Template
This template builds the Three Tier VPC based on the above topology using the built-in cloudformation functions recently mentioned. 

[Gtz4all Three Tier VPC Template](gtz4all-three-tier-vpc.yml)

#### Cloudformation Nested Functions
The above template is prone to error depending on the user inputs. Howerver there are complicated ways to reduce some of them.

* ***Problem***: 
  - Assume user provides a CidrBlock: ```10.168.64.0/22``` and CidrBits:```8```. In order to generate enough Cidr Blocks for 4 AZs, the templace is requesting ```12``` smaller blocks. Since The ```/22``` CidrBlock only contains 4 ```/24's```, the cloudformation stack build will fail.

* ***Solution***: 
  - One possible solution would be to pick from a list of ```AllowedValues``` in the ```Parameters`` section and use ```Conditions``` to set ```CidrBits``` based on user choice using nested conditions in ```resources```.

```yaml
Conditions: 
  CidrBit8:    !Equals [ !Ref Cidr, 20 ]
  CidrBit7:    !Equals [ !Ref Cidr, 21 ]
  CidrBit6:    !Equals [ !Ref Cidr, 22 ]

Resources: 
  !If [CidrBit8, 8, !If [CidrBit7, 7, 6]]
```

* ***How it works***:
  - ```!If [CidrBit8, 8, !If [CidrBit7, 7, 6]]``` 
  
```python
    if CidrBit8 == True:
      cidrBits == 8
    elif CidrBit7 == True:
      cidrBits == 7
    else:
      cidrBits = 6
```

[Gtz4all Three Tier VPC Enhanced Nested Conditions Sample](gtz4all-three-tier-vpc-nested-conditions.yml)
